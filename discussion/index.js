//objective:Create a server-side app using Express Web Framework.


//[section]create a Runtime environment that automatically autofix all the changes in our app.
//were going to use a utility called nodemon.
//upon starting the entry point module with nodemon you will be able to the 'append' the application with the proper run time environmnent. allowing you to save time and effort upon commiting changes to your app.


//step 2
//[section]Append the entire app to our node package manager
    //package.json -> the "heart" of every node project. this also contains different metadata that describes the structure of the project.
    //scripts -> is used to declare and describe custom commands and keyword that can be used to execute this project with the correct runtime environment

    //NOTE: "start" is globally recognize amongst node projects and frameworks as the default command script to execute a task.
    //however for *unconventional* keywords or command you have to append the command "run"
    //SYNTAX: npm run <custom  command>



const expresss = require("express");



console.log("This will be our server");
console.log("NEW CHANGES");
console.log("hello world!");
console.log("this is a new changes");
//you can even insert items like text art into your run time environment
console.log (` 
Welcome to our Express API Server
─────────▀▀▀▀▀▀──────────▀▀▀▀▀▀▀
──────▀▀▀▀▀▀▀▀▀▀▀▀▀───▀▀▀▀▀▀▀▀▀▀▀▀▀
────▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀──────────▀▀▀
───▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀──────────────▀▀
──▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀──────────────▀▀
─▀▀▀▀▀▀▀▀▀▀▀▀───▀▀▀▀▀▀▀───────────────▀▀
─▀▀▀▀▀▀▀▀▀▀▀─────▀▀▀▀▀▀▀──────────────▀▀
─▀▀▀▀▀▀▀▀▀▀▀▀───▀▀▀▀▀▀▀▀──────────────▀▀
─▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀
─▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀
─▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀
──▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀
───▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀▀
─────▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀▀
──────▀▀▀▀▀▀▀▀▀▀▀───▀▀▀────────▀▀▀
────────▀▀▀▀▀▀▀▀▀──▀▀▀▀▀────▀▀▀▀
───────────▀▀▀▀▀▀───▀▀▀───▀▀▀▀
─────────────▀▀▀▀▀─────▀▀▀▀
────────────────▀▀▀──▀▀▀▀
──────────────────▀▀▀▀
───────────────────▀▀
`);
